## Description
Precorconnect footer for AngularJS.

## Example
refer to the [example app](example) for working example code.

## Setup
**install via jspm**  
```shell
jspm install bitbucket:precorconnect/footer-for-angularjs
``` 

**import & wire up**
```js
import 'footer-for-angularjs'

angular.module(
        "app",
        ["footer.module"]);
```