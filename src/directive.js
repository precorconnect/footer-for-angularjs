import template from "./footer.html!text";
import "./footer.css!";
import FooterController from "./controller"

function footer() {
    return {
        restrict: "EA",
        scope: {},
        controller: FooterController,
        controllerAs: "controller",
        bindToController: true,
        template: template
    }
}

export default footer;