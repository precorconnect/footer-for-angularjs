export default class FooterController {

    _year = new Date().getFullYear();

    get year() {
        return this._year;
    }

}