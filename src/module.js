import angular from "angular";
import directive from "./directive";

const footerModule =
    angular
        .module("footer.module", [])
        .directive(directive.name, directive);

export default footerModule;