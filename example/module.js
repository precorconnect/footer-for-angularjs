import angular from "angular";
import footerModule from "../src/module"

const exampleAppModule =
    angular
        .module(
        "exampleApp.module",
        [
            "footer.module"
        ]);

export default exampleAppModule;
